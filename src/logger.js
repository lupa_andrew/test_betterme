require('app-module-path').addPath(__dirname);
const winston = require('winston');
const _ = require('lodash');
const config = require('config');

const label = 'APP';
const isPersonal = config.util.getEnv('NODE_ENV') === 'personal';

const format = [
    winston.format.label({ label, message: false }),
];

if(isPersonal) {
    format.push(
        winston.format.timestamp({
            format: 'HH:mm:ss.SSS',
        }),
        winston.format.colorize({
            all: true,
        }),
        winston.format.metadata({
            key: 'meta',
            fillExcept: ['timestamp', 'label', 'level', 'message'],
        }),
        winston.format.printf((info) => {
            let message = `${info.timestamp} ${info.level}: [${info.label}] ${info.message}`;
            if(_.isEmpty(info.meta)) return message;
            message = `${message} ${JSON.stringify(info.meta, null, ' ')}`;
            if(!info.stack) return message;
            return `${message}\n${info.stack}`;
        }),
    );
} else {
    format.push(
        winston.format.timestamp(),
        winston.format.errors({ stack: true }),
        winston.format.json(),
    );
}
const logger = winston.createLogger({
    level: config.get('log.level'),
    format: winston.format.combine(...format),
    transports: [
        new winston.transports.Console({
            silent: config.get('log.silent'),
        }),
    ],
});

module.exports = logger;
