const _ = require('lodash');
const log = require('logger');
const request = require('request');

const SERVER_URL = 'https://api.github.com';

const LIMIT_EXCEEDED_PREFIX = 'API rate limit exceeded';


class Github {
    async repositoriesSearch(name, sort, order, page, perPage) {
        return this.__sendRequest('GET', '/search/repositories', {
            q: name,
            sort,
            order,
            page,
            per_page: perPage,
        });
    }

    __sendRequest(method, url, parameters) {
        method = method.toUpperCase();

        const requestData = {
            headers: {
                'user-agent': 'betterme_test_app',
                'Accept': 'application/vnd.github.mercy-preview+json,application/vnd.github.v3+json',
            },
            url: `${SERVER_URL}${url}`,
            method,
        };
        if(method === 'GET') requestData.qs = parameters;
        if(method === 'POST') requestData.body = JSON.stringify(parameters);
        return new Promise((resolve, reject) => {
            request(requestData, handleReq(resolve, reject));
        });
    }
}


function handleReq(resolve, reject) {
    return (err, response, body) => {
        let res;
        try {
            res = JSON.parse(body);
        } catch (e) {
            log.error('GITHUB_RESPONSE:PARSE_ERROR', { body });
            return reject(e);
        }
        try {
            if(_.startsWith(res.message, LIMIT_EXCEEDED_PREFIX)) {
                throw new Error('RATE_LIMIT_EXCEEDED');
            }
            return resolve(res);
        } catch (e) {
            return reject(e);
        }
    };
}

module.exports = new Github();
