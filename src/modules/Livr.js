const _ = require('lodash');
const LIVR = require('livr');

LIVR.Validator.defaultAutoTrim(true);


function toNonNegativeInteger() {
    return (value, params, outputArr) => {
        if(value === undefined || value === null || value === '') return null;
        const num = _.toNumber(value);
        if(_.isInteger(num) && num >= 0) {
            outputArr.push(Math.trunc(num));
            return null;
        }
        return 'MUST_BE_NON_NEGATIVE_INTEGER';
    };
}


LIVR.Validator.registerDefaultRules({ to_non_negative_integer: toNonNegativeInteger });


module.exports = LIVR;
