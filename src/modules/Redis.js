const config = require('config');
const Redis = require('ioredis');

function getConnection() {
    return new Redis(config.get('redis'));
}

module.exports = {
    getConnection,
};
