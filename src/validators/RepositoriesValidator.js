const { policyConstructor } = require('./helpers');

const MAX_QUERY_LIMIT = 50;
const DEFAULT_QUERY_LIMIT = 30;
const SORT_COLUMNS = ['stars', 'updated', 'forks', 'help-wanted-issues'];
const SORT_ORDERS = ['asc', 'desc'];


module.exports.search = policyConstructor({
    name: ['string', 'required'],
    limit: ['positive_integer', { max_number: MAX_QUERY_LIMIT }, { default: DEFAULT_QUERY_LIMIT }],
    skip: ['to_non_negative_integer', { default: 0 }],
    sort: ['string', { one_of: SORT_COLUMNS }, { default: 'stars' }],
    order: ['string', { one_of: SORT_ORDERS }, { default: 'desc' }],
}, 'query');
