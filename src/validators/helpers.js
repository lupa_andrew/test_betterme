const _ = require('lodash');
const { HttpError } = require('errors');
const LIVR = require('modules/Livr');


module.exports.policyConstructor = function (rules, dataLocation, extraRules = []) {
    const validator = new LIVR.Validator(rules);
    _.each(extraRules, (rule) => {
        validator.registerRules(rule);
    });
    return (req, res, next) => {
        const rawData = _.get(req, dataLocation, {});
        const validData = validator.validate(rawData);
        if(validData) {
            _.set(req, dataLocation, validData);
            return next();
        }
        return res.error(new HttpError.BadRequest('VALIDATION_FAILED', validator.getErrors()));
    };
};


