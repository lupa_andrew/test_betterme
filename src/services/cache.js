const _ = require('lodash');
const log = require('logger');
const redis = require('modules/Redis').getConnection();


const CACHE_SIZE = 20;

module.exports = {
    createCacheConfiguration,
};

function createCacheConfiguration(cacheName, cacheLimit = CACHE_SIZE) {

    return {
        async get(key) {
            const data = await redis.get(key);
            if(_.isEmpty(data)) return null;
            if(!_.isString(data)) return null;
            try {
                return JSON.parse(data);
            } catch (e) {
                log.error('CACHE:CAN_NOT_READ_FROM', { cacheName, key, data });
                return null;
            }
        },
        async set(key, data) {
            let cacheSize = await redis.llen(cacheName);
            while (cacheSize >= cacheLimit) {
                const keyToRemove = await redis.rpop(cacheName);
                log.info('CACHE:REMOVE_OLD_DATA', { cacheName, keyToRemove });
                await redis.del(keyToRemove);
                cacheSize = await redis.llen(cacheName);
            }
            await Promise.all([
                redis.lpush(cacheName, key),
                redis.set(key, JSON.stringify(data)),
            ]);
        },
    };
}
