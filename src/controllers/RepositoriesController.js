const log = require('logger');
const Github = require('modules/Github');
const cache = require('services/cache');


const REPOSITORIES_CACHE_NAME = 'REPOSITORIES_CACHE';


module.exports.search = async function ({ query }) {
    const name = query.name;
    const sort = query.sort;
    const order = query.order;
    const page = Math.floor(query.skip / query.limit) + 1;
    const limit = query.limit;

    const responseObj = {
        items: [],
        total: 0,
        skip: query.skip,
        limit,
    };

    const cacheKey = `${name}:${sort}:${order}:${page}:${limit}`;
    const repositoriesCache = cache.createCacheConfiguration(
        REPOSITORIES_CACHE_NAME);

    const cachedResult = await repositoriesCache.get(cacheKey);
    if(cachedResult) {
        log.info('GET_FROM_CACHE', {
            total: cachedResult.total_count,
            items: cachedResult.items.length,
        });
        responseObj.items = cachedResult.items;
        responseObj.total = cachedResult.total_count;
        return responseObj;
    }

    const response = await Github.repositoriesSearch(
        name, sort, order, page, limit);

    await repositoriesCache.set(cacheKey, response);

    responseObj.items = response.items;
    responseObj.total = response.total_count;
    return responseObj;
};
