const { HttpError } = require('../errors');
const log = require('logger');

module.exports = function (data) {
    const error = HttpError.constructor.toHttpError(data);
    const content = error.getContent();
    const res = this;
    res.status(content.code);
    log.error(data);
    return res.json(content);
};
