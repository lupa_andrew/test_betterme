module.exports = {
    log: {
        level: 'debug',
        silent: false,
    },

    redis: {
        port: 6379,
        host: 'redis',
        db: 0,
    },

    keepAliveTimeout: 10 * 1000,
    headersTimeout: 15 * 1000,

    port: 9000,
};
