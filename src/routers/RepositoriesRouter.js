const express = require('express');
const repositories = require('controllers/RepositoriesController');
const validator = require('validators/RepositoriesValidator');
const { jsonResponse } = require('./helpers');

const router = new express.Router();


router.get('/api/v1/repositories/search', validator.search, jsonResponse(repositories.search));

module.exports = router;
