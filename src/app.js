require('app-module-path').addPath(__dirname);

const _ = require('lodash');
const bodyParser = require('body-parser');
const express = require('express');

const config = require('config');
const log = require('logger');
const responses = require('responses');
const router = require('routers');


const app = express();
const server = require('http').Server(app);

server.keepAliveTimeout = config.get('keepAliveTimeout');
server.headersTimeout = config.get('headersTimeout');

app.locals.config = config;

// Parsers
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ extended: true, limit: 1024 * 1024, type: ['application/json', 'text/plain'] }));

// Responses
app.use((req, res, next) => {
    _.extend(res, responses);
    next();
});

// Routers
router(app);

server.listen(config.get('port'), () => {
    log.info('--------------------------------------------');
    log.info(`Environment: ${config.util.getEnv('NODE_ENV')}`);
    log.info(`Port: ${config.get('port')}`);
    log.info('--------------------------------------------');
});

process.on('uncaughtException', (error) => {
    log.error('UNCAUGHT_EXCEPTION', error);
});

module.exports = app;
