
**REQUIREMENTS:**

Installed [docker](https://docs.docker.com/) >= 1.10 and [docker-compose](https://docs.docker.com/compose/) >= 1.8


**RUN:**

1. Run `npm i` in `src` directory
2. Run `docker-compose up` in project `/`

**ENDPOINTS**

GET `/api/v1/repositories/search`

Params: `name` valid string for searching repos, `limit` and `skip` - paging params, `sort` and `order` - params for additional search options.
