FROM node:10.15.0

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

RUN npm i -g nodemon

ADD src /usr/src/app


ENV NODE_ENV=personal

EXPOSE 9000
ENTRYPOINT [ "nodemon", "app.js", "--", "-i", "2" ]
